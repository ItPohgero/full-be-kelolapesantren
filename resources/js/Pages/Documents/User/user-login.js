import React from "react";
import App from "../../../Layouts/App";
import Endpoint from "../../Components/Endpoint";
import Header from "../../Components/Header";
import Method from "../../Components/Method";
import Body from "../../Components/Body";
import Params from "../../Components/Params";

function Index(props) {
    return (
        <App>
            <p className="text-lg font-bold uppercase tracking-widest">
                User Login
            </p>
            <p className="text-sm">
                Endpoint ini digunakan untuk mengambil informasi user is login
            </p>
            <Endpoint>
                <p>api/user/logged-in</p>
            </Endpoint>
            <Method>
                <p>GET</p>
            </Method>
            <Header>
                <div className="grid grid-cols-2">
                    <p>Authorization</p>
                    <p>Bearer token</p>
                </div>
            </Header>
            <Body>
                <p>Null</p>
            </Body>
            <Params>
                <p>Null</p>
            </Params>
        </App>
    );
}

export default Index;
