import React from "react";
import App from "../../../Layouts/App";
import Endpoint from "../../Components/Endpoint";
import Header from "../../Components/Header";
import Method from "../../Components/Method";
import Body from "../../Components/Body";
import Params from "../../Components/Params";

function Index(props) {
    return (
        <App>
            <p className="text-lg font-bold uppercase tracking-widest">
                Create new blog
            </p>
            <p className="text-sm">
                Endpoint ini digunakan untuk menambahkan data blog baru
            </p>
            <Endpoint>
                <p>api/blog/create</p>
            </Endpoint>
            <Method>
                <p>POST</p>
            </Method>
            <Header>
                <div className="grid grid-cols-2">
                    <p>Accept</p>
                    <p>application/json</p>
                </div>
                <div className="grid grid-cols-2">
                    <p>Authorization</p>
                    <p>Bearer token</p>
                </div>
            </Header>
            <Body>
                <div className="grid grid-cols-2 mb-2">
                    <p>title</p>
                    <div>
                        <p className="text-xs">* Required atau wajib diisi</p>
                        <p className="text-xs">
                            * String atau jenis inputan karakter adalah string
                        </p>
                        <p className="text-xs">* Maksimal 255 karakter</p>
                    </div>
                </div>
                <div className="grid grid-cols-2 mb-2">
                    <p>content</p>
                    <div>
                        <p className="text-xs">* Required atau wajib diisi</p>
                        <p className="text-xs">
                            * String atau jenis inputan karakter adalah string
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-2 mb-2">
                    <p>thumbnail</p>
                    <div>
                        <p className="text-xs">* Tipe format : jpeg, png, jpg, gif, dan svg</p>
                        <p className="text-xs">
                            * Ukuran maksimal 2048 kb
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-2 mb-2">
                    <p>category_id</p>
                    <div>
                        <p className="text-xs">* Relasi dengan <i>categories table</i></p>
                    </div>
                </div>
            </Body>
            <Params>
                <p>Null</p>
            </Params>
        </App>
    );
}

export default Index;
