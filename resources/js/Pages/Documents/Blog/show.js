import React from "react";
import App from "../../../Layouts/App";
import Endpoint from "../../Components/Endpoint";
import Header from "../../Components/Header";
import Method from "../../Components/Method";
import Body from "../../Components/Body";
import Params from "../../Components/Params";

function Index(props) {
    return (
        <App>
            <p className="text-lg font-bold uppercase tracking-widest">
                Show blogs
            </p>
            <p className="text-sm">
                Endpoint ini digunakan untuk menampilkan data spesifik blog berdasarkan slug
            </p>
            <Endpoint>
                <p>
                    api/blog/<span className="text-red-500">user_id</span>/
                    <span className="text-green-500">slug</span>
                </p>
            </Endpoint>
            <Method>
                <p>GET</p>
            </Method>
            <Header>
                <p>Null</p>
            </Header>
            <Body>
                <p>Null</p>
            </Body>
            <Params>
                <p>
                    Parameter user_id diterima seperti tanda merah pada endpoint
                </p>
                <p>
                    Parameter slug (blog) diterima seperti tanda hijau pada
                    endpoint
                </p>
            </Params>
        </App>
    );
}

export default Index;
