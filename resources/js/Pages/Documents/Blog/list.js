import React from "react";
import App from "../../../Layouts/App";
import Endpoint from "../../Components/Endpoint";
import Header from "../../Components/Header";
import Method from "../../Components/Method";
import Body from "../../Components/Body";
import Params from "../../Components/Params";

function Index(props) {
    return (
        <App>
            <p className="text-lg font-bold uppercase tracking-widest">
                List blogs
            </p>
            <p className="text-sm">
                Endpoint ini digunakan untuk menampilkan daftar blogs
            </p>
            <Endpoint>
                <p>
                    api/blogs/<span className="text-red-500">user_id</span>
                </p>
            </Endpoint>
            <Method>
                <p>GET</p>
            </Method>
            <Header>
                <p>Null</p>
            </Header>
            <Body>
                <p>Null</p>
            </Body>
            <Params>
                <p>Parameter user_id diterima seperti tanda merah pada endpoint</p>
            </Params>
        </App>
    );
}

export default Index;
