import React from "react";
import App from "../../../Layouts/App";
import Endpoint from "../../Components/Endpoint";
import Header from "../../Components/Header";
import Method from "../../Components/Method";
import Body from "../../Components/Body";
import Params from "../../Components/Params";

function Index(props) {
    return (
        <App>
            <p className="text-lg font-bold uppercase tracking-widest">
               Login to api user pesantren
            </p>
            <p className="text-sm">
                Endpoint ini digunakan untuk request token user untuk digunakan
                sebagai credentials aplikasi
            </p>
            <Endpoint>
                <p>api/login</p>
            </Endpoint>
            <Method>
                <p>GET</p>
            </Method>
            <Header>
                <div className="grid grid-cols-2">
                    <p>Accept</p>
                    <p>application/json</p>
                </div>
            </Header>
            <Body>
                <div className="grid grid-cols-2 mb-2">
                    <p>email</p>
                    <div>
                        <p className="text-xs">* Required atau wajib diisi</p>
                        <p className="text-xs">* String atau jenis inputan karakter adalah string</p>
                        <p className="text-xs">* Email atau wajib menggunakan format email</p>
                    </div>
                </div>
                <div className="grid grid-cols-2 mb-2">
                    <p>password</p>
                    <div>
                        <p className="text-xs">* Required atau wajib diisi</p>
                        <p className="text-xs">* String atau jenis inputan karakter adalah string</p>
                        <p className="text-xs">* Minimal 6 karakter</p>
                    </div>
                </div>
            </Body>
            <Params>
                <p>Null</p>
            </Params>
        </App>
    );
}

export default Index;
