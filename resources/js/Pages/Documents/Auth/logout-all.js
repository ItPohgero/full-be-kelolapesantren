import React from "react";
import App from "../../../Layouts/App";
import Endpoint from "../../Components/Endpoint";
import Header from "../../Components/Header";
import Method from "../../Components/Method";
import Body from "../../Components/Body";
import Params from "../../Components/Params";

function Index(props) {
    return (
        <App>
            <p className="text-lg font-bold uppercase tracking-widest">
                Logout or revoked all tokens from api
            </p>
            <p className="text-sm">
                Endpoint ini digunakan untuk revoked all tokens user pesantren
                pesantren
            </p>
            <Endpoint>
                <p>api/logout-all</p>
            </Endpoint>
            <Method>
                <p>POST</p>
            </Method>
            <Header>
                <div className="grid grid-cols-2">
                    <p>Authorization</p>
                    <p>Bearer token</p>
                </div>
            </Header>
            <Body>
                <p>Null</p>
            </Body>
            <Params>
                <p>Null</p>
            </Params>
        </App>
    );
}

export default Index;
