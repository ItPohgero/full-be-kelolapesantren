import React from "react";

function Params({ children }) {
    return (
        <div className=" border-b pb-2 mb-2 border-dashed">
            <p className="font-bold text-xs tracking-widest mt-4 uppercase">
                Params
            </p>
            <div className=" tracking-normal leading-relaxed text-gray-500 text-sm">{children}</div>
        </div>
    );
}

export default Params;
