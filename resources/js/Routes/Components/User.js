import React from "react";
import { Link } from "@inertiajs/inertia-react";

function User() {
    const file = "/user";
    return (
        <>
            <div className="ml-4">
                <div className="my-2">
                    <Link
                        className="hover:text-red-500"
                        href={`${file}/user-login`}
                    >
                        Is Login
                    </Link>
                </div>
            </div>
        </>
    );
}

export default User;
