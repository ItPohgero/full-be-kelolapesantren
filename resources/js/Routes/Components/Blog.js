import React from "react";
import { Link } from "@inertiajs/inertia-react";

function Blog() {
    const file = "/blog";
    return (
        <>
            <div className="ml-4">
                <div className="my-2">
                    <Link href={`${file}/create`}>New Blog</Link>
                </div>
            </div>
            <div className="ml-4">
                <div className="my-2">
                    <Link href={`${file}/edit`}>Update Blog</Link>
                </div>
            </div>
            <div className="ml-4">
                <div className="my-2">
                    <Link href={`${file}/list`}>List Blogs</Link>
                </div>
            </div>
            <div className="ml-4">
                <div className="my-2">
                    <Link href={`${file}/show`}>Show Spesifik Blog</Link>
                </div>
            </div>
        </>
    );
}

export default Blog;
