import React from "react";
import { Link } from "@inertiajs/inertia-react";

function Auth() {
    const file = "/auth";
    return (
        <>
            <div className="ml-4">
                <div className="my-2">
                    <Link className="hover:text-red-500" href={`${file}/login`}>Login Request</Link>
                </div>
                <div className="my-2">
                    <Link className="hover:text-red-500" href={`${file}/logout`}>Logout on Devices</Link>
                </div>
                <div className="my-2">
                    <Link className="hover:text-red-500" href={`${file}/logout-all`}>Logout All Devices</Link>
                </div>
            </div>
        </>
    );
}

export default Auth;
