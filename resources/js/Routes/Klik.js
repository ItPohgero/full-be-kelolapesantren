import React from "react";
import { Link } from "@inertiajs/inertia-react";
import Auth from "./Components/Auth";
import Blog from "./Components/Blog";
import User from "./Components/User";

function Klik() {
    return (
        <div className="capitalize text-sm">
            <div className="my-2 uppercase font-bold">
                <Link href="/">Introduction</Link>
            </div>
            <div>
                <p className="font-bold uppercase">Auth</p>
                <Auth />
            </div>
            <div>
                <p className="font-bold uppercase">User</p>
                <User />
            </div>
            <div>
                <p className="font-bold uppercase">Blogs</p>
                <Blog />
            </div>
        </div>
    );
}

export default Klik;
