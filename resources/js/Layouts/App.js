import React, { useState } from "react";
import Klik from "../Routes/Klik";

function App({ children }) {
    const [menuMobile, setMenuMobile] = useState(false);
    return (
        <div className="min-h-screen flex justify-center bg-white pb-32">
            <div className="w-full md:w-2/3 md:border-x">
                <div className="mb-4 sticky bg-gradient-to-r from-pink-50 via-blue-50 to-red-50 top-0 bg-opacity-80 py-4 items-center px-4 flex justify-between md:block md:justify-center gap-4">
                    <div className="md:text-center">
                        <p className="font-bold lowercase">
                            Api <span className="text-red-500">kelola</span>
                            pesantren.com
                        </p>
                        <p className="text-xs">
                            Documentasi api kelolapesantren.com
                        </p>
                        <p style={{fontSize: 10}}>for free to use</p>
                    </div>
                    <div className="block md:hidden">
                        <button onClick={() => setMenuMobile(!menuMobile)}>
                            <img src={!menuMobile ? `/align-right.svg` : `/cross-small.svg`} className="w-4 h-4" />
                        </button>
                    </div>
                </div>
                <div className="px-4 md:px-10 mt-2 md:mt-10">
                    <div className="block md:flex md:gap-10">
                        <div className="hidden md:block w-2/6 border-r border-dashed">
                            <Klik />
                        </div>
                        <div className="w-full md:w-4/6">{children}</div>
                    </div>
                </div>
                {menuMobile && (
                    <div className="block md:hidden fixed w-2/3 top-0 left-0">
                        <div className="bg-white bg-opacity-95 shadow-lg min-h-screen p-6 overflow-y-auto h-64">
                            <Klik />
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}

export default App;
