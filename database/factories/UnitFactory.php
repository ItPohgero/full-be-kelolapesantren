<?php

namespace Database\Factories;

use App\Models\Pesantren;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Unit>
 */
class UnitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pesantren_id'      => Pesantren::factory(),
            'name'              => $this->faker->name(),
            'slug'              => str()->slug($this->faker->name()),
            'head'              => $this->faker->name(),
        ];
    }
}
