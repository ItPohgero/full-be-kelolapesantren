<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pesantren>
 */
class PesantrenFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'       => User::factory(),
            'name'          => $this->faker->sentence(2),
            'head'          => $this->faker->userName(),
            'address'       => $this->faker->address(),
            'phone'         => $this->faker->phoneNumber(),
        ];
    }
}
