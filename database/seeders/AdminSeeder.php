<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'              => 'admin by it pohgero',
            'email'             => 'itpohgero@gmail.com',
            'email_verified_at' => now(),
            'password'          => bcrypt('#Pohgero01'),
            'remember_token'    => str()->random(10),
            'role'              => 'admin',
        ]);
    }
}
