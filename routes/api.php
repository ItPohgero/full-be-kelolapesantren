<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PesantrenController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', fn () => 'this url endpoint for kelolapesantren.com');

/**
 * Authentication route api 
 */
Route::post('/login', [AuthController::class, 'login']);
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/logout-all', [AuthController::class, '_logout']);
});

/**
 * Public route api  
 */
# Route blogs
Route::get('/blogs/{user}', [BlogController::class, 'index']);
Route::get('/blog/{user}/{blog:slug}', [BlogController::class, 'show']);

# Route Category
Route::get('/category/{category:slug}', [CategoryController::class, 'index']);


/**
 * Routes for needed authentication
 * user or role pesantren
 */
Route::middleware('auth:sanctum')->group(function () {
    # Check wo is logged in user
    Route::get('/user/logged-in', [UserController::class, 'loggedin']);

    # Route units
    Route::post('/unit/create', [UnitController::class, 'create']);
    Route::delete('/unit/delete/{unit:slug}', [UnitController::class, 'delete']);

    # Route blogs
    Route::post('/blog/create', [BlogController::class, 'create']);
    Route::patch('/blog/edit/{blog:slug}', [BlogController::class, 'edit']);
});

/**
 * Routes for admin
 */

Route::middleware('auth:sanctum', 'admin')->group(function () {
    # user
    Route::post('/user/new', [UserController::class, 'new']);
    Route::delete('/user/delete/{user}', [UserController::class, 'destroy']);
});
