<?php

use App\Http\Controllers\Doc\DocumentationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DocumentationController::class, 'index']);
Route::get('/auth/{file}', [DocumentationController::class, 'auth']);
Route::get('/user/{file}', [DocumentationController::class, 'user']);
Route::get('/blog/{file}', [DocumentationController::class, 'blog']);