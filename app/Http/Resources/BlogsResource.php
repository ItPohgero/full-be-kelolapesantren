<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class BlogsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'slug'          => $this->slug,
            'content'       => $this->content,
            'thumbnail'     => env("APP_URL") . Storage::url($this->thumbnail),
            'category'      => [
                'title'     => $this->category->title,
                'slug'      => $this->category->slug,
            ],
            'time_humans'   => $this->created_at->diffForHumans(),
            'created_at'    => $this->created_at->toDateTimeString(),
        ];
    }
}
