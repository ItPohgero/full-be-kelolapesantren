<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    use ApiResponser;

    public function create(Request $request)
    {
        $attr = $request->validate([
            'name'  => 'required|string|max:255',
            'head'  => 'required|string|max:255',
        ]);

        $user = $request->user();
        $attr['slug']   = str()->slug($attr['name'] . '-' . str()->random(10));
        $unit = $user->pesantren->units()->create($attr);

        return $this->success([
            'pesantren'     => $user->pesantren->name,
            'new unit'          => $unit,
            'all units'         => $user->pesantren->units,
        ], 'Unit created');
    }

    public function delete(Request $request, $unit)
    {
        $user = $request->user();
        $data = $user->pesantren->units()->whereSlug($unit)->firstOrFail();
        $data->delete();

        return $this->success([
            'pesantren'     => $user->pesantren->name,
            'deleted unit'  => $data,
            'all units'     => $user->pesantren->units,
        ], 'Unit deleted');
    }
}
