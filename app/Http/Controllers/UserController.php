<?php

namespace App\Http\Controllers;

use App\Http\Resources\SingleUserReqource;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ApiResponser;

    public function new(Request $request)
    {
        $attr = $request->validate([
            'name'          => 'required|string|max:255',
            'email'         => 'required|string|email|unique:users,email',
            'password'      => 'required|string|min:6|confirmed',
            'pes_name'      => 'required',
            'pes_head'      => 'required',
            'pes_address'   => 'required',
            'pes_phone'     => 'required',
        ]);

        $user = User::create([
            'name'          => $attr['name'],
            'password'      => bcrypt($attr['password']),
            'email'         => $attr['email'],
            'role'          => 'pesantren'
        ]);

        $user->pesantren()->create([
            'name'          => $attr['pes_name'],
            'head'          => $attr['pes_head'],
            'address'       => $attr['pes_address'],
            'phone'         => $attr['pes_phone'],
        ]);

        $token = $user->createToken('Api Tokens Kelolapesantren');

        return $this->success([
            'token_type'    => 'Bearer',
            'token'         => $token->plainTextToken,
            'user'          => [
                'name'          => $user->name,
                'email'         => $user->email
            ],
        ], 'Authorized', 200);
    }

    public function loggedin(Request $request)
    {
        return $this->success([
            'user'  =>  new SingleUserReqource($request->user())
        ], 'Currently logged in');
    }

    public function destroy(User $user)
    {

        if ($user->role == 'admin' || $user->id == 1) {
            return $this->error('Error request', 500, [
                'force'     => 'Nothing'
            ]);
        }

        $user->delete();
        return $this->success([
            'user' => $user
        ], 'User has been deleted');
    }
}
