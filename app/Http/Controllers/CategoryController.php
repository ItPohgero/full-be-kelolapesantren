<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryBlogsResource;
use App\Models\Category;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ApiResponser;
    public function index(Category $category)
    {
        return $this->success([
            'category' => new CategoryBlogsResource($category)
        ], "Blogs category retrieved successfully");
    }
}
