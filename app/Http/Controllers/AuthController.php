<?php

namespace App\Http\Controllers;

use App\Models\Pesantren;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponser;
    public function login(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::attempt($attr)) {
            return $this->error('Credentials not match', 401);
        }

        return $this->success([
            'token' => $request->user()->createToken('Api Tokens Kelolapesantren')->plainTextToken
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     * custom token on devices to prevent unauthorized access
     */
    public function logout(Request $request)
    {

        $request->user()->currentAccessToken()->delete();
        return [
            'message' => 'Tokens revoked on this device'
        ];
    }

    /**
     * revoke all token related to user
     */
    public function _logout(Request $request)
    {

        $request->user()->tokens()->delete();
        return [
            'message' => 'All tokens revoked on your account'
        ];
    }
}
