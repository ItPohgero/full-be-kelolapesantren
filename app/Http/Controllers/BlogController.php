<?php

namespace App\Http\Controllers;

use App\Http\Resources\BlogsResource;
use App\Models\Blog;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    use ApiResponser;

    public function index(User $user)
    {
        return $this->success([
            'blogs' => BlogsResource::collection($user->blogs)
        ], "Blogs retrieved successfully");
    }

    public function show(User $user, Blog $blog)
    {
        return $this->success([
            'blogs' => new BlogsResource($blog)
        ], "Blog show retrieved successfully");
    }

    public function create(Request $request)
    {
        $attr = $request->validate([
            'title'         => 'required|string|max:255',
            'content'       => 'required|string',
            'thumbnail'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id'   => 'required|numeric',
        ]);

        $attr['slug'] = str()->slug($attr['title'] . "-" . str()->random(5));
        $request->user()->blogs()->create($attr);

        return $this->success([
            'data' => $attr
        ], "Blog has been created");
    }

    public function edit(Request $request, Blog $blog)
    {
        $attr = $request->validate([
            'title'         => 'required|string|max:255',
            'content'       => 'required|string',
            'thumbnail'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id'   => 'required|numeric',

        ]);

        $attr['slug'] = str()->slug($attr['title'] . "-" . str()->random(5));
        $blog->update($attr);

        return $this->success([
            'data' => $attr
        ], "Blog has been updated");
    }
}
