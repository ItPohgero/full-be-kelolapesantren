<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;

class DocumentationController extends Controller
{
    public function index()
    {
        return inertia('Documents/Index');
    }

    public function auth($file)
    {
        return inertia('Documents/Auth/' . $file,);
    }

    public function user($file)
    {
        return inertia('Documents/User/' . $file,);
    }
    public function blog($file)
    {
        return inertia('Documents/Blog/' . $file,);
    }
}
