<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesantren extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'head', 'address', 'phone'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'head' => $this->head,
            'address' => $this->address,
            'phone' => $this->phone,
        ];
    }

    public function units(){
        return $this->hasMany(Unit::class);
    }
}
