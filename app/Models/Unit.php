<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    protected $guarded;
    public function pesantren()
    {
        return $this->belongsTo(Pesantren::class);
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'head' => $this->head,
        ];
    }
}
